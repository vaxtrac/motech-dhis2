package org.motechproject.dhis2.repository;

import org.motechproject.dhis2.domain.StageDataElement;
import org.motechproject.mds.annotations.Lookup;
import org.motechproject.mds.annotations.LookupField;
import org.motechproject.mds.service.MotechDataService;

/**
 * MDS data service for {@link org.motechproject.dhis2.domain.StageDataElement}
 */
public interface StageDataElementDataService extends MotechDataService<StageDataElement> {

    @Lookup
    StageDataElement findByUuid(@LookupField(name = "uuid") String uuid);
}
