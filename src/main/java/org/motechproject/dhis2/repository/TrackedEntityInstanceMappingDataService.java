package org.motechproject.dhis2.repository;

import java.util.List;

import org.motechproject.dhis2.domain.TrackedEntityInstanceMapping;
import org.motechproject.mds.annotations.Lookup;
import org.motechproject.mds.annotations.LookupField;
import org.motechproject.mds.service.MotechDataService;

/**
 * MDS data service for {@link TrackedEntityInstanceMapping}
 */
public interface TrackedEntityInstanceMappingDataService extends MotechDataService<TrackedEntityInstanceMapping> {
    @Lookup
    TrackedEntityInstanceMapping findByExternalName(@LookupField(name = "externalName") String externalName);
    
    @Lookup
    TrackedEntityInstanceMapping findByDhis2Uuid(@LookupField(name = "dhis2Uuid") String uuid);
    
    @Lookup
    List<TrackedEntityInstanceMapping> findByEnrollmentStatus(
    		@LookupField(name = "enrollmentStatus") Boolean enrollmentStatus);
}
