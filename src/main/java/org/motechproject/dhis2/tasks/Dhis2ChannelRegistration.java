package org.motechproject.dhis2.tasks;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.motechproject.tasks.ex.ValidationException;

import org.motechproject.dhis2.service.TasksService;

@Component("dhis2ChannelRegistration")
public class Dhis2ChannelRegistration {

    private static final Logger LOGGER = LoggerFactory.getLogger(Dhis2ChannelRegistration.class);

    private BundleContext bundleContext;
    private TasksService tasksService;
    

    @Autowired
    public Dhis2ChannelRegistration(BundleContext bundleContext, TasksService tasksService) {
        this.bundleContext = bundleContext;
        this.tasksService = tasksService;
    }

    public void cleanTasksUp() {
	LOGGER.info("Remove channel");
	tasksService.removeChannel();
    }

    public void updateTasksInfo() {
	LOGGER.info("Updating tasks integration");
	try {
	    updateChannel();
	} catch (ValidationException e) {
	    LOGGER.error("Channel generated was not accepted by tasks due to validation errors", e);
	}
    }
    
    public void updateChannel() {
	ServiceReference serviceReference = bundleContext.getServiceReference("org.motechproject.tasks.service.ChannelService");
        if (serviceReference != null) {
            Object service = bundleContext.getService(serviceReference);
            if (service != null) {
                LOGGER.info("Registering DHIS2 tasks channel with the channel service");
                tasksService.updateChannel();
            } else {
                LOGGER.warn("No channel service present, channel not registered");
            }
        }
    }
}

