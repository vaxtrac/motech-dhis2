package org.motechproject.dhis2.domain;

import org.motechproject.mds.annotations.Access;
import org.motechproject.mds.annotations.Entity;
import org.motechproject.mds.annotations.Field;
import org.motechproject.mds.util.SecurityMode;
import java.util.Objects;
import javax.jdo.annotations.Unique;

/**
 * Represents a DHIS2 Stage Data Element
 */
@Entity
@Access(value = SecurityMode.PERMISSIONS, members = {"configureDhis"})
public class StageDataElement {
    @Field(required = true)
    @Unique
    private String uuid;
    
    @Field
    private DataElement dataElement;


    public StageDataElement() { }

    public StageDataElement(String uuid) {
	this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
    /*
    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
	this.stage = stage;
    }
    */
    public DataElement getDataElement() {
	return dataElement;
    }
    
    public void setDataElement(DataElement dataElement) {
	this.dataElement = dataElement;
    }

    public int hashCode() {
        return Objects.hash(uuid);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final StageDataElement other = (StageDataElement) obj;
        return Objects.equals(this.uuid, other.uuid);
    }

}
