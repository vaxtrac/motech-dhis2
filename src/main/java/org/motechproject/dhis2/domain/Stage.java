package org.motechproject.dhis2.domain;

import org.motechproject.mds.annotations.Access;
import org.motechproject.mds.annotations.Entity;
import org.motechproject.mds.annotations.Field;
import org.motechproject.mds.util.SecurityMode;
import java.util.Objects;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.Unique;
import java.util.List;
import java.util.LinkedList;
import java.util.Set;
import javax.jdo.annotations.Element;
import javax.jdo.annotations.Join;

/**
 * Represents a DHIS2 program stage event
 */
@Entity(maxFetchDepth = 2)
@Access(value = SecurityMode.PERMISSIONS, members = {"configureDhis"})
public class Stage {
    @Field(required = true)
    @Unique
    private String uuid;

    @Field
    private String name;

    @Field
    private List<StageDataElement> stageDataElements;

    @Field
    private String program;

    @Field
    private boolean registration;

    public boolean hasRegistration() {
        return registration;
    }

    public void setRegistration(boolean registration) {
        this.registration = registration;
    }

    
    public List<StageDataElement> getStageDataElements() {
        return stageDataElements;
    }

    public void setStageDataElements(List<StageDataElement> stageDataElements) {
        this.stageDataElements = stageDataElements;
    }

    /*
    public List<DataElement> getDataElements() {
	List<DataElement> dataElements = new LinkedList<DataElement>();
	for (StageDataElement stageDataElement : stageDataElements) {
	    dataElements.add(stageDataElement.getDataElement());
	}
	return dataElements;
    }
    
    public void setDataElements(List<DataElement> dataElements) {
      this.dataElements = dataElements;
    }
    */
    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid, name);
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj) {
            return true;
        }
	if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final Stage other = (Stage) obj;
	return Objects.equals(this.uuid, other.uuid) &&
	    Objects.equals(this.name, other.name);
    }
}
