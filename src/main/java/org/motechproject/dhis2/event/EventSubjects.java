package org.motechproject.dhis2.event;

public final class EventSubjects {

    private EventSubjects() {
    }
    
    public static final String BASE = "org.motechproject.dhis2.";
    public static final String CREATE_ENTITY = "create_entity";
    public static final String ENROLL_IN_PROGRAM = "enroll_in_program";
    public static final String UPDATE_PROGRAM_STAGE = "update_program_stage";
    public static final String CREATE_AND_ENROLL = "create_and_enroll";
    public static final String SEND_DATA_VALUE = "send_data_value";
    public static final String SEND_DATA_VALUE_SET = "send_data_value_set";
    public static final String STAGE_UPDATE_SUCCESS = BASE + "stage_update_success";
    public static final String STAGE_UPDATE_FAILURE = BASE + "stage_update_failure";
    public static final String ENROLLMENT_SUCCESS = BASE + "enrollment_success";
    public static final String FAILED_DHIS2_DELIVERY = BASE + "failed_dose_delivery";
}
