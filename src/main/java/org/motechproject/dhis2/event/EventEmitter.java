package org.motechproject.dhis2.event;

import org.motechproject.event.MotechEvent;
import org.motechproject.event.listener.EventRelay;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;


@Component
public class EventEmitter {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private EventRelay eventRelay;

	public EventEmitter() { }

	public EventEmitter(EventRelay eventRelay) {
		this.eventRelay = eventRelay;
	}

	public void emitEvent(String subject, Map<String, Object> eventParams) {
		MotechEvent motechEvent = new MotechEvent(subject, eventParams);
		eventRelay.sendEventMessage(motechEvent);
		String message = "Emitted an event with subject " + subject;
		logger.info(message);
	}

	public void emitStageUpdateSuccessEvent(String externalId, String stage, String date, String doseCaseId) {
		Map<String, Object> eventParams = new HashMap<String, Object>();
		eventParams.put(EventParams.EXTERNAL_ID, externalId);
		eventParams.put(EventParams.STAGE, stage);
		eventParams.put(EventParams.DATE, date);
		eventParams.put(EventParams.DOSE_CASE_ID, doseCaseId);
		emitEvent(EventSubjects.STAGE_UPDATE_SUCCESS, eventParams);
	}

	public void emitEnrollmentSuccessEvent(String externalId) {
		Map<String, Object> eventParams = new HashMap<String, Object>();
		eventParams.put(EventParams.EXTERNAL_ID, externalId);
		emitEvent(EventSubjects.ENROLLMENT_SUCCESS, eventParams);
	}   

	public void emitFailedDeliveryEvent(String requestBody, String errorMessage) {
		Map<String, Object> eventParams = new HashMap<String, Object>();
		eventParams.put(EventParams.ERROR_MESSAGE, errorMessage);
		eventParams.put(EventParams.REQUEST_BODY, requestBody);
		emitEvent(EventSubjects.FAILED_DHIS2_DELIVERY, eventParams);
	}
}
