package org.motechproject.dhis2.service;

import java.util.List;

import org.motechproject.dhis2.domain.TrackedEntityInstanceMapping;

/**
 * Manages CRUD operations for a {@link TrackedEntityInstanceMapping}
 */
public interface TrackedEntityInstanceMappingService extends GenericCrudService<TrackedEntityInstanceMapping> {
    TrackedEntityInstanceMapping findByExternalId(String externalId);
    TrackedEntityInstanceMapping findByDhis2Uuid(String uuid);
    List<TrackedEntityInstanceMapping> findByEnrollmentStatus(boolean status);
    TrackedEntityInstanceMapping create(String externalId, String dhisId);
    TrackedEntityInstanceMapping create(String externalId, String dhisId, String healthArea, boolean enrollmentStatus);
    String mapFromExternalId(String externalId);
}
