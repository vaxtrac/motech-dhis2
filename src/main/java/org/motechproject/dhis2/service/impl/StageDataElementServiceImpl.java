package org.motechproject.dhis2.service.impl;

import org.motechproject.dhis2.domain.StageDataElement;
import org.motechproject.dhis2.repository.StageDataElementDataService;
import org.motechproject.dhis2.rest.domain.ProgramStageDataElementDto;
import org.motechproject.dhis2.service.StageDataElementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Implementation of {@link org.motechproject.dhis2.service.StageDataElementService}
 */
@Service("stageDataElementService")
public class StageDataElementServiceImpl implements StageDataElementService {
    @Autowired
    private StageDataElementDataService stageDataElementDataService;

    @Override
    public List<StageDataElement> findAll() {
        return stageDataElementDataService.retrieveAll();
    }

    @Override
    public StageDataElement findById(String id) {
        return stageDataElementDataService.findByUuid(id);
    }

    @Override
    public StageDataElement createFromDetails(ProgramStageDataElementDto details) {
	StageDataElement stageDataElement = new StageDataElement();
        stageDataElement.setUuid(details.getId());
        return stageDataElementDataService.create(stageDataElement);
    }

    @Override
    public void update(StageDataElement stageDataElement) {
        stageDataElementDataService.update(stageDataElement);
    }

    @Override
    public void delete(StageDataElement stageDataElement) {
        stageDataElementDataService.delete(stageDataElement);
    }

    @Override
    public void deleteAll() {
        stageDataElementDataService.deleteAll();
    }
}
