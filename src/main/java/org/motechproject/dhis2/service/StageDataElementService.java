package org.motechproject.dhis2.service;

import org.motechproject.dhis2.domain.StageDataElement;
import org.motechproject.dhis2.rest.domain.ProgramStageDataElementDto;

/**
 * Manages CRUD operations for a {@link org.motechproject.dhis2.domain.StageDataElement}
 */
public interface StageDataElementService extends GenericCrudService<StageDataElement> {
    StageDataElement findById(String id);
    StageDataElement createFromDetails(ProgramStageDataElementDto details);
    void update(StageDataElement dataElement);
}
